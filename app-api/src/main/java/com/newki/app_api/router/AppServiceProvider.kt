package com.newki.app_api.router

import com.alibaba.android.arouter.launcher.ARouter

//ARouter 的组件服务提供
object AppServiceProvider {

    var appService: IAppService? = ARouter.getInstance().navigation(IAppService::class.java)

}