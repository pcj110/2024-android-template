package com.newki.app_api.entity

data class AndroidVersion(val code: String, val url: String)
