package com.newki.profile_api.router

import com.alibaba.android.arouter.facade.template.IProvider
import com.android.basiclib.bean.OkResult
import com.newki.profile_api.entity.TopArticleBean
import com.newki.profile_api.entity.UserProfile

interface IProfileService : IProvider {

    suspend fun fetchUserProfile(): OkResult<List<TopArticleBean>>
}
