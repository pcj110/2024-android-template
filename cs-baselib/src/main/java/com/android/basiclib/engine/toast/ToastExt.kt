package com.android.basiclib.engine.toast

import android.view.Gravity
import com.android.basiclib.R
import com.hjq.toast.Toaster
import com.hjq.toast.style.BlackToastStyle


/** toast使用引擎 **/

fun Any.toast(msg: String?) {
    Toaster.setStyle(BlackToastStyle())
    Toaster.setGravity(Gravity.BOTTOM, 0, 180)
    Toaster.show(msg)
}

fun Any.toast(res: Int) {
    Toaster.setStyle(BlackToastStyle())
    Toaster.setGravity(Gravity.BOTTOM, 0, 180)
    Toaster.show(res)
}

fun Any.toastError(msg: String?) {
    Toaster.setView(R.layout.custom_toast_failed_view)
    Toaster.setGravity(Gravity.CENTER)
    Toaster.show(msg)
}

fun Any.toastError(res: Int) {
    Toaster.setView(R.layout.custom_toast_failed_view)
    Toaster.setGravity(Gravity.CENTER)
    Toaster.show(res)
}

fun Any.toastSuccess(msg: String?) {
    Toaster.setView(R.layout.custom_toast_success_view)
    Toaster.setGravity(Gravity.CENTER)
    Toaster.show(msg)
}

fun Any.toastSuccess(res: Int) {
    Toaster.setView(R.layout.custom_toast_success_view)
    Toaster.setGravity(Gravity.CENTER)
    Toaster.show(res)
}

fun Any.cancelToast() {
    Toaster.cancel()
}