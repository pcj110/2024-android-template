package com.newki.template.ui.fargment

import android.os.Bundle
import com.android.basiclib.base.fragment.BaseVVDLoadingFragment
import com.android.basiclib.base.vm.EmptyViewModel
import com.android.basiclib.engine.toast.toast
import com.android.basiclib.ext.click
import com.android.basiclib.utils.CommUtils
import com.newki.template.databinding.FragmentLoad1Binding

/**
 * 默认的LoadingFragment
 */
class Load1Fragment : BaseVVDLoadingFragment<EmptyViewModel, FragmentLoad1Binding>() {

    companion object {
        fun obtainFragment(): Load1Fragment {
            return Load1Fragment()
        }
    }

    override fun init(savedInstanceState: Bundle?) {

        initData()

        mBinding.tvPageTitle.click {
            toast("点击标题 - 第一个Fragment")
        }

    }

    private fun initData() {
        //模拟的Loading的情况
        showStateLoading()

        CommUtils.getHandler().postDelayed({

            showStateSuccess()

        }, 1500)

    }


}
